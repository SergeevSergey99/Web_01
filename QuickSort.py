def sort(Array):
    less = []
    equal = []
    more = []
    if len(Array) > 1:
        pivot = Array[0]
        for x in Array:
            if x < pivot:
                less.append(x)
            if x == pivot:
                equal.append(x)
            if x > pivot:
                more.append(x)
        return sort(less) + equal + sort(more)
    else:
        return Array
Mas = [5, 8 ,42, 7 ,9 , 2, 4]
print sort(Mas)
